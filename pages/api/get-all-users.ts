import { User } from "@prisma/client";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from "../../lib/prisma";

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<User[]>
) {
    prisma.user.findMany().then(response => {
        console.log(response);
        res.status(200).json(response);
    })
}
