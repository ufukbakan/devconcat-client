import { PrismaClient } from "@prisma/client"

let anyglobal = global as any;

if(!anyglobal.prisma){
    anyglobal.prisma = new PrismaClient();
    global = anyglobal;
}

export default anyglobal.prisma as PrismaClient;