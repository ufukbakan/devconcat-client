import { useEffect, useState } from "react";
import { prcTimeout } from "precision-timeout-interval";

type TypeAnimationTextProps = {
    children: string,
    speed: number,
    once: boolean
}

export default function TypeAnimationText(props: TypeAnimationTextProps) {
    const maxWidth = props.children.length;
    const [currentWidth, setCurrentWidth] = useState(0);

    useEffect(() => {
        if (!(props.once && currentWidth >= maxWidth)) {
            // setTimeout(() => {
            //     if (currentWidth < maxWidth) {
            //         setCurrentWidth((prev) => prev + 1);
            //     } else {
            //         setCurrentWidth(0);
            //     }
            // }, 400 / props.speed);
            prcTimeout(400 / props.speed, () => {
                if (currentWidth < maxWidth) {
                    setCurrentWidth((prev) => prev + 1);
                } else {
                    setCurrentWidth(0);
                }
            });
        }
    }, [currentWidth])

    return (
        <span style={{
            height: "1.5em",
            lineHeight: "1.5em",
            maxWidth: currentWidth + "ch",
            display: "inline-block",
            overflow: "hidden",
            fontFamily: "var(--font-mono)"
        }}>
            {props.children}
        </span>
    )
}

TypeAnimationText.defaultProps = {
    speed: 1,
    once: false
}