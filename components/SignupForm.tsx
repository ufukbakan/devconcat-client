import { Button, Input, VStack } from "@chakra-ui/react";

export default function SignupForm() {



    return (
        <form>
            <VStack spacing={2} padding={10}>
                <Input name="username" placeholder="Username" />
                <Input name="email" placeholder="Email" />
                <Input name="email-again" placeholder="Email again" />
                <Input type="password" name="password" placeholder="Password" />
                <Input type="password" name="password-again" placeholder="Password again" />
                <Button colorScheme="linkedin">Sign Up</Button>
            </VStack>
        </form>
    )
}