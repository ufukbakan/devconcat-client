import { Box, Button, Center, Input, VStack } from "@chakra-ui/react";

export default function LoginForm() {
    return (
        <form>
            <VStack spacing={5} padding={10}>
                <Input size="md" type="text" placeholder="Username" name="username" />
                <Input size="md" type="password" placeholder="Password" name="password" />
                <Button variant="solid" colorScheme="linkedin">Login</Button>
            </VStack>
        </form>
    )
}