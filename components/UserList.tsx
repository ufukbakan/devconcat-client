import prisma from "../lib/prisma"
import { User } from "@prisma/client";
import { useEffect, useState } from "react";
import axios from "axios";

export default function UserList() {
    const [users, setUsers] = useState<User[]>([]);
    function fetchUsers() {
        axios.get<User[]>("/api/get-all-users").then((response) => setUsers(response.data));
    }

    useEffect(() => {
        fetchUsers();
    }, [])

    return (
        <ol className="text-gray-50">
            {users.map(user => (
                <li key={user.id}>{JSON.stringify(user)}</li>
            ))}
        </ol>
    )
}