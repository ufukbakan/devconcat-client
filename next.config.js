/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  cleanDistDir: true
}

module.exports = nextConfig
